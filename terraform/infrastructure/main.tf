/*
 * Creates a IAM resources for my litecoin project. wink*
 */
module "iam" {
  source = "../modules/iam"
  region = "ap-southeast-2"
  name   = "litecoin"
}