/*
 * The special terraform configuration block type is used to configure
 * some behaviors of Terraform itself, such as requiring a minimum 
 * Terraform version to apply your configuration.
 */

terraform {
  required_version = ">= 0.14"
  # experiments      = [module_variable_optional_attrs]
  /*
   * Each Terraform configuration can specify a backend, which defines
   * exactly where and how operations are performed, where state snapshots
   * are stored, etc. Most non-trivial Terraform configurations configure
   * a remote backend so that multiple people can work with the same 
   * infrastructure.
   */
  backend "s3" {
    bucket = "jrpospos-tfremotestate"
    key    = "litecoin-iam"
    region = "ap-southeast-2"
  }
  /*
   * Each Terraform module must declare which providers it requires,
   * so that Terraform can install and use them. Provider requirements
   * are declared in a `required_providers` block.
   */
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}