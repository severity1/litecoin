/*
 * Output values make information about your infrastructure available on the command line,
 * and can expose information for other Terraform configurations to use. Output values are
 * similar to return values in programming languages.
 */

output "role_arn" {
  description = "IAM Role's ARN."
  value       = module.iam.role_arn
}

output "policy_arn" {
  description = "The IAM Policy's ARN."
  value       = module.iam.policy_arn
}

output "group_arn" {
  description = "The IAM Group's ARN."
  value       = module.iam.group_arn
}

output "user_arn" {
  description = "The IAM User's ARN."
  value       = module.iam.user_arn
}