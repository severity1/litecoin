data "aws_caller_identity" "this" {}

/*
 * Create IAM Role which allows anyone in the account to assume role.
 */
resource "aws_iam_role" "this" {
  name = format("%s-ci-role", var.name)

  assume_role_policy = jsonencode({
    Version   = "2012-10-17"
    Statement = [
      {
        Action    = "sts:AssumeRole"
        Effect    = "Allow"
        Principal = { 
          "AWS" = format("arn:aws:iam::%s:root", data.aws_caller_identity.this.account_id)
        }
      },
    ]
  })

  tags = {
    Project = var.name
  }
}

/*
 * Create IAM Policy
 */
resource "aws_iam_policy" "this" {
  name = format("%s-ci-policy", var.name)

  policy = jsonencode({
    Version   = "2012-10-17"
    Statement = [
      {
        Effect   = "Allow",
        Action   = "sts:AssumeRole",
        Resource = aws_iam_role.this.arn
      },
    ]
  })

  tags = {
    Project = var.name
  }
}

/*
 * Create IAM Group
 */
resource "aws_iam_group" "this" {
  name = format("%s-ci-group", var.name)
  path = format("/%s-ci-group/", var.name)
}

/*
 * Attach IAM Policy to IAM Group
 */
resource "aws_iam_group_policy_attachment" "this" {
  group      = aws_iam_group.this.name
  policy_arn = aws_iam_policy.this.arn
}

/*
 * Create IAM User
 */
resource "aws_iam_user" "this" {
  name = format("%s-ci-user", var.name)
  path = format("/%s-ci-user/", var.name)

  tags = {
    Project = var.name
  }
}

/*
 * Add IAM User to IAM Group
 */
resource "aws_iam_user_group_membership" "this" {
  user = aws_iam_user.this.name

  groups = [
    aws_iam_group.this.name,
  ]
}