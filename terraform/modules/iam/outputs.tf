/*
 * Output values make information about your infrastructure available on the command line,
 * and can expose information for other Terraform configurations to use. Output values are
 * similar to return values in programming languages.
 */

output "role_arn" {
  description = "IAM Role's ARN."
  value       = aws_iam_role.this.arn
}

output "policy_arn" {
  description = "The IAM Policy's ARN."
  value       = aws_iam_policy.this.arn
}

output "group_arn" {
  description = "The IAM Group's ARN."
  value       = aws_iam_group.this.arn
}

output "user_arn" {
  description = "The IAM User's ARN."
  value       = aws_iam_user.this.arn
}