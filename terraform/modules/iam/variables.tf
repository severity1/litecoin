/*
 * Input variables serve as parameters for a Terraform module, allowing aspects of
 * the module to be customized without altering the module's own source code, and
 * allowing modules to be shared between different configurations.
 */

variable "region" {
  description = "Region to use."
  type        = string
  default     = null
}

variable "name" {
  description = "Name to use."
  type        = string
  default     = null
}