#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This is a modified copy from https://stackoverflow.com/questions/50037369/find-and-replace-string-from-multiple-files-in-a-folder-using-python
import glob
import ntpath
import os

output_dir = "output"

# This creates output directory if it does not exist
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# This will replace keyword in each file matching the glob and write the output file into the output dir.
for f in glob.glob("input/*.yml"):
    with open(f, 'r') as inputfile:
        with open('%s/%s' % (output_dir, ntpath.basename(f)), 'w') as outputfile:
            for line in inputfile:
                outputfile.write(line.replace('8.8.8.8', '1.1.1.1'))