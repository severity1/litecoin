#!/bin/env bash

# I won't be fancy here and will just do basic text manipulation stuff.
# I want to replace a string in multiple files using grep and sed.

# This does a recursive copy of input to output directory
cp -pr input output

# Grep just does a recursive search for keyword in output dir and only displays
#   files if keyword exists then result gets piped into xargs which makes the
#   grep result as input to sed. sed then just replaces keywords for the grep results.
grep -lr '8.8.8.8' output/ | xargs sed -i 's/8.8.8.8/1.1.1.1/g'