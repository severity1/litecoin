# This is a modified copy of lodow/litecoin and https://github.com/domyrtille/interview_project
# This section downloads litecoin archive and verifies it.

# Specifies the base image I want to use, in this case ubuntu:focal and label this as "build" (good for multi-stage build,
#   Just remembered that this was on the technical interview.)
FROM ubuntu:20.04 as build

ARG LTC_VERSION="0.18.1"
ARG LTC_KEYID="FE3348877809386C"

# Runs the apt package manager to install wget and gpg, then do a cleanup afterwards to keep the image lean.
RUN apt-get update && apt-get install -y wget gpg \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Downloads archive and the PGP electronic signature ASCII file for litecoin 0.18.1
RUN wget https://download.litecoin.org/litecoin-${LTC_VERSION}/linux/litecoin-${LTC_VERSION}-x86_64-linux-gnu.tar.gz \
  && wget https://download.litecoin.org/litecoin-${LTC_VERSION}/linux/litecoin-${LTC_VERSION}-linux-signatures.asc

# Fetch public key from keyserver using the key ID.
RUN gpg --keyserver pgp.mit.edu --recv-key ${LTC_KEYID} || gpg --keyserver keyserver.ubuntu.com --recv-key ${LTC_KEYID} \
  # Verify the PGP electronic signature ASCII file
  && gpg --verify litecoin-${LTC_VERSION}-linux-signatures.asc \
  # Verify the archive using the signature, this will use the sha256 of the archive and check if it's in the linux signature file.
  && grep $(sha256sum litecoin-${LTC_VERSION}-x86_64-linux-gnu.tar.gz | awk '{ print $1 }') litecoin-${LTC_VERSION}-linux-signatures.asc \
  # Unpacks the litecoin archive if passed verification.
  && mkdir /tmp/litecoin \
  && tar -xzvf litecoin-${LTC_VERSION}-x86_64-linux-gnu.tar.gz -C /tmp/litecoin --strip=2

# This section copies litecoin 0.18.1 from "build".
FROM ubuntu:20.04

ARG LTC_VERSION="0.18.1"

# For security reason we remove the login and /etc/passwd informations(--gecos)
RUN mkdir /litecoin \
    && adduser --home /litecoin --shell /sbin/nologin --gecos '' --no-create-home --disabled-password --uid 10001 ltc \
    && chown -R ltc:ltc /litecoin

# Copy litecoind from "build" stage to this stage.
COPY --chown=root:ltc --chmod=0450 --from=build /tmp/litecoin/litecoind /usr/local/bin/litecoind

# Runs the container as normal user ltc.
USER ltc

EXPOSE 9332 9333

# Ensures that working directory used is/litecoin
WORKDIR /litecoin

# This executes litecoind when cointainer is run.
CMD ["litecoind"]