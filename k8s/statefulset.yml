---
# This is a modified copy of https://github.com/domyrtille/interview_project
# This creates a Namespace resource
apiVersion: v1
kind: Namespace
metadata:
  name: litecoin
---
# This creates a Statefulset resource
apiVersion: apps/v1
kind: StatefulSet
metadata:
  namespace: litecoin
  name: litecoin
  labels:
    app: litecoin
    version: '0.18.1'
spec:
  # Weird behavior but, Statefulsets require a service defined but it doesn't seem
  #   like it requires the actual service to exist based on tests.
  serviceName: litecoin-svc
  selector:
    # Should be the same as the container name
    matchLabels:
      app: litecoin
  replicas: 1
  template:
    metadata:
      labels:
        app: litecoin
    spec:
      # https://kubernetes.io/docs/concepts/policy/pod-security-policy/
      securityContext:
        # User as 10001 ID to avoid conflicts with the host
        runAsUser: 10001
        runAsGroup: 10001
        fsGroup: 10001 # all processes of the container are also part of the supplementary group ID 10001
      # This setting defines how long should Kubernetes wait after sending the shutdown signal to the pod before forcing deleting it. 
      terminationGracePeriodSeconds: 10
      containers:
      - name: litecoin
        image: 'registry.gitlab.com/severity1/litecoin:latest'
        command: ["litecoind"]
        args: ["-datadir=/litecoin/data", "-maxmempool=50"]
        imagePullPolicy: Always
        resources:
          requests:
            cpu: 500m
            memory: 512Mi
          limits:
            cpu: 1000m
            memory: 1024Mi
        # Check if the container is ready to be used
        readinessProbe:
          tcpSocket:
            port: ltc-port
          initialDelaySeconds: 5
          timeoutSeconds: 3
          periodSeconds: 3
        ports:
          - name: ltc-port
            containerPort: 9333
          - name: ltc-rpc
            containerPort: 9332
        volumeMounts:
        - name: litecoin-data
          mountPath: /litecoin/data
        # Tighten security
        securityContext:
          readOnlyRootFilesystem: true
          allowPrivilegeEscalation: false
          # Ensure that container is not running with potentially unnecessary privileges
          #   For Reference: https://snyk.io/security-rules/SNYK-CC-K8S-6/container-does-not-drop-all-default-capabilities/
          capabilities:
            drop:
             - ALL
  # Opted to use volumeClaimTemplates to simplify PV provisioning.
  # For Reference:
  #   - https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/#volume-claim-templates
  #   - https://zhimin-wen.medium.com/persistent-volume-claim-for-statefulset-8050e396cc51
  volumeClaimTemplates:
  - metadata:
      name: litecoin-data
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 100Gi