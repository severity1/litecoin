# litecoin

## Summary


| Content    | Description |
| ---------- | ----------- |
| Dockerfile | Builds the litecoin image. |
| k8s/       | Used to deploy statefulset in kubernetes. |
| ./gitlab/agents/docker-desktop-agent/config.yaml | Authorizes kubernetes agent to access my litecoin project. |
| .gitlab-ci.yml | defines the cicd pipeline. The pipeline builds the litecoin image and publishes it to gitlab registry then deploys the statefulset using the published image into my kubernetes cluster. |
| text-manipulation/bulk_replace_keyword.sh | Shell script that uses grep and sed to do text manipulation. |
| text-manipulation/bulk_replace_keyword.py | Python script that does the same text manipulation as the shell script mentioned above. |
| terraform/modules/iam | Basic module that creates an IAM role, a policy that assumes the role, a group that uses the policy, a user that is a member of the role. |
| terraform/infrastructure | Uses the moudules/iam to provision IAM resources in my AWS account also uses s3 bucket as remote state. |


